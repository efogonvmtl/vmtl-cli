#!/usr/bin/env Node

import {CreateCommand} from "./commands/create/create-command";

const fs = require("fs");
/**
 * CLI runner class
 * 
 * @export
 * @class Cli
 */
export class Cli {
    /**
     * Creates an instance of app.
     * 
     */
    constructor() {

    }
    /**
     * Run command
     * 
     * @param {any} argv
     */
    run(argv) {
        const command = argv[2] ? argv[2].toString().toLowerCase() : undefined;
        switch (command) {
            case "create":
                const createCmd = new CreateCommand();
                createCmd.run(argv, function (err, result) { process.exit(0) });
                break;
            default:
                console.error("Command unknown: %s", command ? command : "NULL");
                console.error("use: vmtl-cli ? where ? can be: create");
                break;
        }
    }
}
const app = new Cli();
app.run(process.argv);