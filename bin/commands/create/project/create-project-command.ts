import {CreateProjectRestfulApi} from "./restfulapi/create-project-restfulapi";
/**
 * Create project command runner
 * 
 * @export
 * @class CreateProjectCommand
 */
export class CreateProjectCommand {
    /**
     * Creates an instance of CreateProjectCommand.
     * 
     */
    constructor() {

    }
    /**
     * Run command
     * 
     * @param {any} argv
     * @param {any} callback
     */
    run(argv, callback) {
        const type = argv[4] ? argv[4].toString().toLowerCase() : undefined;
        switch (type) {
            case "restfulapi":
                const createCmd = new CreateProjectRestfulApi();
                createCmd.run(argv, callback);
                break;
            default:
                console.error("Create type unknown: %s", type ? type : "NULL");
                console.error("use: vmtl-cli create project ? where ? can be: RestfulApi");
                break;
        }
    }
}
