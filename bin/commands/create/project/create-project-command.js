"use strict";
const create_project_restfulapi_1 = require("./restfulapi/create-project-restfulapi");
/**
 * Create project command runner
 *
 * @export
 * @class CreateProjectCommand
 */
class CreateProjectCommand {
    /**
     * Creates an instance of CreateProjectCommand.
     *
     */
    constructor() {
    }
    /**
     * Run command
     *
     * @param {any} argv
     * @param {any} callback
     */
    run(argv, callback) {
        const type = argv[4] ? argv[4].toString().toLowerCase() : undefined;
        switch (type) {
            case "restfulapi":
                const createCmd = new create_project_restfulapi_1.CreateProjectRestfulApi();
                createCmd.run(argv, callback);
                break;
            default:
                console.error("Create type unknown: %s", type ? type : "NULL");
                console.error("use: vmtl-cli create project ? where ? can be: RestfulApi");
                break;
        }
    }
}
exports.CreateProjectCommand = CreateProjectCommand;
//# sourceMappingURL=create-project-command.js.map