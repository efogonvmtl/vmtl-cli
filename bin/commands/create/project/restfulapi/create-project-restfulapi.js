"use strict";
const readdir = require("recursive-readdir");
const path = require("path");
const fs = require("fs");
const fsExtra = require("fs-extra");
const shell = require("shelljs");
const mkdirp = require("mkdirp");
/**
 * Create restful api project
 *
 * @export
 * @class CreateProjectRestfulApi
 */
class CreateProjectRestfulApi {
    /**
     * Creates an instance of CreateProjectRestfulApi.
     *
     */
    constructor() {
    }
    rmdirAsync(path, callback) {
        fs.readdir(path, (err, files) => {
            if (err) {
                // Pass the error on to callback
                callback(err, []);
                return;
            }
            var wait = files.length, count = 0, folderDone = (err) => {
                count++;
                // If we cleaned out all the files, continue
                if (count >= wait || err) {
                    fs.rmdir(path, callback);
                }
            };
            // Empty directory to bail early
            if (!wait) {
                folderDone(null);
                return;
            }
            // Remove one or more trailing slash to keep from doubling up
            path = path.replace(/\/+$/, "");
            files.forEach((file) => {
                var curPath = path + "/" + file;
                fs.lstat(curPath, (err, stats) => {
                    if (err) {
                        callback(err, []);
                        return;
                    }
                    if (stats.isDirectory()) {
                        this.rmdirAsync(curPath, folderDone);
                    }
                    else {
                        fs.unlink(curPath, folderDone);
                    }
                });
            });
        });
    }
    ;
    /**
     * Run command
     *
     * @param {any} argv
     * @param {any} callback
     */
    run(argv, callback) {
        const name = argv[5] ? argv[5].toString().toLowerCase() : undefined;
        const dest = argv[6] ? argv[6].toString().toLowerCase() : undefined;
        let folderPath = path.join("bin", "files", "create-project-restfulapi");
        if (!name) {
            throw new Error("Name not provided");
        }
        let data = { "project": name };
        let pathTo = path.join(process.cwd().toString(), "tmp/src");
        mkdirp(pathTo, (err) => {
            if (err) {
                throw new Error(err);
            }
            shell.exec(`git clone https://bitbucket.org/efogonvmtl/vmtl-cli-restfulapi ${pathTo}`);
            readdir(pathTo, (err, files) => {
                files.forEach(file => {
                    let newFilePath = file.replace(path.join("tmp", "src"), dest);
                    fsExtra.copySync(file, newFilePath);
                });
                this.rmdirAsync(pathTo, (err) => {
                    shell.exec(`npm install -g tsd typescript tslint`);
                    shell.exec(`tsd install node`);
                });
            });
        });
    }
}
exports.CreateProjectRestfulApi = CreateProjectRestfulApi;
//# sourceMappingURL=create-project-restfulapi.js.map