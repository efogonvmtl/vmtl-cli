import {CreateProjectCommand} from "./project/create-project-command";
/**
 * Handles create command
 * 
 * @export
 * @class CreateCommand
 */
export class CreateCommand {
    /**
     * Creates an instance of CreateCommand.
     * 
     */
    constructor() {

    }
    /**
     * Run command
     * 
     * @param {any} argv
     * @param {any} callback
     */
    run(argv, callback) {
        const type = argv[3] ? argv[3].toString().toLowerCase() : undefined;
        switch (type) {
            case "project":
                const createCmd = new CreateProjectCommand();
                createCmd.run(argv, callback);
                break;
            default:
                console.error("Create type unknown: %s", type ? type : "NULL");
                console.error("use: vmtl-cli create ? where ? can be: project");
                break;
        }
    }
}