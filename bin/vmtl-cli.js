#!/usr/bin/env Node
"use strict";
const create_command_1 = require("./commands/create/create-command");
const fs = require("fs");
/**
 * CLI runner class
 *
 * @export
 * @class Cli
 */
class Cli {
    /**
     * Creates an instance of app.
     *
     */
    constructor() {
    }
    /**
     * Run command
     *
     * @param {any} argv
     */
    run(argv) {
        const command = argv[2] ? argv[2].toString().toLowerCase() : undefined;
        switch (command) {
            case "create":
                const createCmd = new create_command_1.CreateCommand();
                createCmd.run(argv, function (err, result) { process.exit(0); });
                break;
            default:
                console.error("Command unknown: %s", command ? command : "NULL");
                console.error("use: vmtl-cli ? where ? can be: create");
                break;
        }
    }
}
exports.Cli = Cli;
const app = new Cli();
app.run(process.argv);
//# sourceMappingURL=vmtl-cli.js.map